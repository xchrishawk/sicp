#lang racket

(define (new-if predicate then-clause else-clause)
  (cond [predicate then-clause]
        [else else-clause]))

(new-if (= 2 3) 0 5)
(new-if (= 1 1) 0 5)

;; Works as expected
(define (factorial-if n)
  (if (= n 1)
      1
      (* n (factorial-if (sub1 n)))))

;; Recurses infinitely due to applicative-order evaluation
(define (factorial n)
  (new-if (= n 1)
          1
          (* n (factorial (sub1 n)))))
