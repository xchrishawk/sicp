#lang racket

(provide find-next-primes time-primes)

(define (find-next-primes prime? base num)
  (define (iterate check-value primes)
    (if (= num (length primes))
        primes
        (if (prime? check-value)
            (iterate (add1 check-value) (cons check-value primes))
            (iterate (add1 check-value) primes))))
  (reverse (iterate base empty)))

(define (time-primes prime?)
  (define iterations 15)
  (let iterate ([iteration 1])
    (let*-values ([(base) (expt 10 iteration)]
                  [(result unused-1 time-ms unused-2) (time-apply find-next-primes (list prime? base 5))])
      (printf "10^~a (~a ms): ~a\n" iteration time-ms result))
    (when (< iteration iterations)
      (iterate (add1 iteration)))))
